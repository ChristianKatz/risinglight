﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// this scirpt is for the currency skilltree and calculates the income of the player
public class Currency : MonoBehaviour
{
    // the time how long the player and the enemy have time to kill soldiers
    private float time = 300;

    // get the number of kills to decide who has won the game
    private FightDestination fightDestination;

    // the ScriptableObjects for the currency
    public ScriptableObjectCurrency[] wageUpgrade;

    // display the current time, currency and the different wins
    [SerializeField]
    private TextMeshProUGUI playTime;
    [SerializeField]
    private TextMeshProUGUI enemyWins;
    [SerializeField]
    private TextMeshProUGUI playerWins;
    [SerializeField]
    private TextMeshProUGUI draw;
    [SerializeField]
    private TextMeshProUGUI currency;

    //Varibale to count the current magic points
    private int magicPoints;
    public int MagicPoints
    {
        get
        {
            return magicPoints;
        }
        set
        {
            magicPoints = value;
        }
    }

    //Variables, which lock the spells if the player wants to buy it again
    bool MerchantIsActivated = false;
    bool SaleEducationIsActivated = false;
    bool VehicleIsActivated = false;

    // Variable, which stops the Magic Points income, if the player uses the pause menu
    bool stopMagicPoints = false;
    public bool StopMagicPoints
    {
        get
        {
            return stopMagicPoints;
        }
        set
        {
            stopMagicPoints = value;
        }
    }

    // after the 5 minutes the menu will show up
    [SerializeField]
    private GameObject menu;

    // the 3 visual Barriers of the buttons for the currency
    [SerializeField]
    private GameObject vehicleBarrier;
    [SerializeField]
    private GameObject merchantBarrier;
    [SerializeField]
    private GameObject saleEducationBarrier;

    void Start()
    {
        // at the beginning of the game the different conditions are deactivated
        enemyWins.gameObject.SetActive(false);
        playerWins.gameObject.SetActive(false);
        draw.gameObject.SetActive(false);

        // get the script
        fightDestination = FindObjectOfType<FightDestination>();

        // at the beginning of the game the 3 visual Barriers of the buttons are deactivated
        vehicleBarrier.SetActive(false);
        saleEducationBarrier.SetActive(false);
        merchantBarrier.SetActive(false);
    }

    void Update()
    {
        // calculation of the time and the conversion in the text variable
        time -= Time.deltaTime;
        playTime.text = string.Format("{0:0}", time);

        // if the time has reached 0 the kills of the enemy and the player will be compared
        // menu will show up
        if(time <= 0)
        {
            menu.SetActive(true);
            Time.timeScale = 0;

            if(fightDestination.PlayerKills < fightDestination.EnemyKills)
            {
                enemyWins.gameObject.SetActive(true);
            }
            if (fightDestination.PlayerKills > fightDestination.EnemyKills)
            {
                playerWins.gameObject.SetActive(true);
            }
            if(fightDestination.PlayerKills == fightDestination.EnemyKills)
            {
                draw.gameObject.SetActive(true);
            }
        }

        //the conversion of the Magic Points in a text variable
        currency.text = string.Format("Magic Points: " + magicPoints);
    }

    // Click button to get Magic Points
    public void ClickForMagicPoints()
    {
        if(stopMagicPoints == false)
        magicPoints++;
    }

    // If the player has enough Magic points to buy the Upgrade the passiv income will be increased
    public void Merchant()
    {
        if(magicPoints >= wageUpgrade[0].cost && MerchantIsActivated == false)
        {
            magicPoints -= wageUpgrade[0].cost;
            StartCoroutine(MerchantIncome());
            merchantBarrier.SetActive(true);
            MerchantIsActivated = true;        
        }       
    }

    // If the player has enough Magic points to buy the Upgrade the passiv income will be increased
    public void SaleEducation()
    {      
        if (magicPoints >= wageUpgrade[1].cost && SaleEducationIsActivated == false)
        {
            magicPoints -= wageUpgrade[1].cost;
            StartCoroutine(SaleEducationIncome());
            saleEducationBarrier.SetActive(true);
            SaleEducationIsActivated = true;                      
        }
    }

    // If the player has enough Magic points to buy the Upgrade the passiv income will be increased
    public void Vehicle()
    {
        if (magicPoints >= wageUpgrade[2].cost && VehicleIsActivated == false)
        {
            magicPoints -= wageUpgrade[2].cost;
            StartCoroutine(VehicleIncome());
            vehicleBarrier.SetActive(true);
            VehicleIsActivated = true;          
        }
    }

    // Enumerators, which increase the passive income every second
    IEnumerator VehicleIncome()
    {
        if(stopMagicPoints == false)
        {
            yield return new WaitForSeconds(wageUpgrade[2].wageCooldownInSeconds);
            magicPoints += wageUpgrade[2].wage;

            StartCoroutine(VehicleIncome());
        }
    }
    IEnumerator MerchantIncome()
    {
       if(stopMagicPoints == false)
       {
            yield return new WaitForSeconds(wageUpgrade[0].wageCooldownInSeconds);
            magicPoints += wageUpgrade[0].wage;

            StartCoroutine(MerchantIncome());
       }
    }
    IEnumerator SaleEducationIncome()
    {
        if (stopMagicPoints == false)
        {          
            yield return new WaitForSeconds(wageUpgrade[1].wageCooldownInSeconds);
            magicPoints += wageUpgrade[1].wage;
            
            StartCoroutine(SaleEducationIncome());
        }
    }
}
