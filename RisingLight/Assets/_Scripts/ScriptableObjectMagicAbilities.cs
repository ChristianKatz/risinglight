﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this ScriptableObject is for the 3 Magic Abilities, which the player is able to control
[CreateAssetMenu(fileName = "Magic Ability", menuName = "ScriptableObjects/ Magic Ability", order = 0)]
public class ScriptableObjectMagicAbilities : ScriptableObject
{
    public int magicDamage;
    public int cooldown;
    public int cost;
    public string triggerTag;

}
