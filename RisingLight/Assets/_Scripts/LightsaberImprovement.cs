﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// script that activates the visual effect of the spell "Increase Damage"
public class LightsaberImprovement : MonoBehaviour
{
    // particle system for the lightaber enhancement
    [SerializeField]
    private GameObject[] increaseDamageVisual;

    // skript of the skilltree to get the information that the spell is activated
    private Skilltree skilltree;

    void Start()
    {
        // Get the script
        skilltree = FindObjectOfType<Skilltree>();            
    }

    void Update()
    {
        // if the spell "IncreaseDamage" is activated the particle system shows up
       if(skilltree.VisualLightsaberIsActivated == true)
        {
            increaseDamageVisual[0].SetActive(true);
            increaseDamageVisual[1].SetActive(true);
            increaseDamageVisual[2].SetActive(true);
            increaseDamageVisual[3].SetActive(true);
            Destroy(this, 2f);
            
        }
    }
}
